import numpy as np

from sklearn import svm
import time
import pandas as pd
from sklearn import metrics




# load dataset into Pandas DataFrame
df = pd.read_csv(".\indian_pines_after_pca.dat")


n=[]
ind=[]
for i in range(2):
    ind.append('PC-'+str(i+1))

features = ind
X = df.loc[:, features].values
# Separating out the target
Y = df.loc[:,['0']].values
Y= np.reshape(Y,(21025))


test =labels = np.load("./terrain/test_data.npy")

newTest=np.reshape(test,(145*145))

clf = svm.SVC(gamma='scale')
clf.fit(X, Y)
start = time.time()
Yhat = clf.predict(X)
end = time.time()
print('Time Taken For Classification is :',(end - start))
print("Accuracy :",metrics.accuracy_score(Yhat, newTest)*100)
print('\n','*'*11,'Accuracy of INDIAN-PINES Dataset Classification SVM After PCA','*'*11)

