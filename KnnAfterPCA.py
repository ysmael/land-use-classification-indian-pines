import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


from sklearn import decomposition
from sklearn import datasets

# load dataset into Pandas DataFrame
df = pd.read_csv(".\indian_pines_after_pca.dat")

from sklearn.preprocessing import StandardScaler
n=[]
ind=[]
for i in range(2):
    ind.append('PC-'+str(i+1))

features = ind
X = df.loc[:, features].values
# Separating out the target
Y = df.loc[:,['0']].values
Y= np.reshape(Y,(21025))
print(Y.shape)
test =labels = np.load("./terrain/test_data.npy")

newTest=np.reshape(test,(145*145))
print(newTest.shape)


from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
import time
#model = KNeighborsClassifier()
model=KNeighborsClassifier(n_neighbors =10000, weights='uniform', algorithm='auto')
model.fit(X, Y)
start = time.time()
Yhat = model.predict(X)
end = time.time()
print('Time Taken For Classification is :',(end - start))
print("Accuracy :",metrics.accuracy_score(Yhat, newTest)*100)
print('\n','*'*11,'Accuracy of INDIAN-PINES Dataset KNN Classification After PCA','*'*11)
print('*'*11,' Classifier : K-NEAREST NEIGHBOUR ','*'*11)
