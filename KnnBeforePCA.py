import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio


from sklearn import decomposition
from sklearn import datasets

def loadIndianPinesData():
    file = "./ValeursReflectance/Indian_pines_corrected.mat"
    data = sio.loadmat(file)['indian_pines_corrected']
    labels = np.load("./terrain/train_data.npy")

    return data, labels

from sklearn.preprocessing import StandardScaler
n=[]
ind=[]
for i in range(200):
    n.append(i+1)
for i in range(200):
    ind.append('px'+str(n[i]))

features = ind
x , y = loadIndianPinesData()

newX = np.reshape(x, (-1, x.shape[2]))
newY = np.reshape(y,(145*145))
test =labels = np.load("./terrain/test_data.npy")

newTest=np.reshape(test,(145*145))

from sklearn.neighbors import KNeighborsClassifier  # FOR K=13 ,IT HAS ACCURACY AROUND 72.7488902980
from sklearn import metrics
import time
#model = KNeighborsClassifier()
model=KNeighborsClassifier(n_neighbors =10000, weights='uniform', algorithm='auto')
model.fit(newX, newY)
start = time.time()
Yhat = model.predict(newX)
end = time.time()
print('Time Taken For Classification is :',(end - start))
print("Accuracy :",metrics.accuracy_score(Yhat, newTest)*100)
print('\n','*'*11,'Accuracy of INDIAN-PINES Dataset Before PCA','*'*11)
print('*'*11,' Classifier : K-NEAREST NEIGHBOUR ','*'*11)
